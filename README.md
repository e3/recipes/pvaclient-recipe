# pvaClient conda recipe

Home: https://github.com/epics-base/pvaClientCPP

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: pvaClient EPICS V4 C++ module
